all: tor.ini output
	pluto build -o output/ -t tor tor.ini

output:
	mkdir -p output

.PHONY: all
